#!/bin/bash

#PBS -l procs=1,pmem=16gb,gpus=1

#### Walltime ####
#PBS -l walltime=5:20:00:00


#PBS -q p100_normal_q
#PBS -A vllab_01

# Access group. Do not change this line.
#PBS -W group_list=newriver

# Uncomment and add your email address to get an email when your job starts, completes, or aborts
#PBS -M zxcve@vt.edu
#PBS -m bea
source activate py36
module load cuda/8.0.44
module load cudnn/6.0
cd /home/sloke/oml/Matrix-Capsules-EM-Tensorflow6
echo $CUDA_VISIBLE_DEVICES
python train.py "smallNORB"
